﻿using System;

namespace DivideZero
{
    public sealed class UsoDivideByZeroException
    {
        public static void Main()
        {
            int dividendo = 13;
            int divisor = 0;

            try
            {
                Console.WriteLine("{0}/{1}", (dividendo / divisor));
            }
            catch (DivideByZeroException)
            {
                Console.WriteLine("Error: Intento de división entre cero.");
            }
            //se lanza cuando se intenta dividir un número entero entre cero (0).
        }
    }
}
