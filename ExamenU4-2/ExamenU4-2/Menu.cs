﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExamenU4_2
{
    abstract class Menu
    {
        public abstract void Ingredientes();
    }
    class Pepperoni : Menu
    {
        public override void Ingredientes()
        {
            Console.WriteLine("*PIZZA PEPPERONI*");
            Console.WriteLine("-Pepperoni\n-Carne\n-Jamon");
        }
    }
    class Hawaiana : Menu
    {
        public override void Ingredientes()
        {
            Console.WriteLine("*PIZZA HAWAIANA*");
            Console.WriteLine("-Jamon\n-Piña\n-Chorizo");
        }
    }
}
