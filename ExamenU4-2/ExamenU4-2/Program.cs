﻿using System;

namespace ExamenU4_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Pepperoni obj1 = new Pepperoni();
            Hawaiana obj2 = new Hawaiana();

            obj1.Ingredientes();
            obj2.Ingredientes();
        }
    }
}
