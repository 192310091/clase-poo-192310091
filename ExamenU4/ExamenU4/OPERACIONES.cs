﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExamenU4
{
    class OPERACIONES
    {
        public int Area(int altura, int Base)
        {
            int res = altura * Base;
            return res;
        }
        public double Area(double altura, double Base)
        {
            double res = altura * Base;
            return res;
        }
        public double Area(double altura, double Base, double c)
        {
            double res = (altura * Base)/c;
            return res;
        }
    }
}
