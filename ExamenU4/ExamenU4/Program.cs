﻿using System;

namespace ExamenU4
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("-SOBRECARGA DE METODOS-");
            OPERACIONES obj = new OPERACIONES();
            int resultado = obj.Area(10, 20);
            Console.WriteLine("Area cuadrado: " + resultado);
            double resultado2 = obj.Area(12.3, 5.5);
            Console.WriteLine("Area rectangulo: " + resultado2);
            double resultado3 = obj.Area(5.2, 13, 2);
            Console.WriteLine("Area triangulo: " + resultado3);
        }
    }
}
