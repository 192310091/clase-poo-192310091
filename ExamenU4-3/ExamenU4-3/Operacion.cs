﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExamenU4_3
{
    class Operacion:Interfaz
    {
        public void Suma(int a,int b)
        {
            int res = a + b;
            Console.WriteLine("Resultado suma: "+res);
        }
        public void Resta(int a,int b)
        {
            int res = a - b;
            Console.WriteLine("Resultado resta: "+res);
        }
    }
}
