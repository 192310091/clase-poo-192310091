﻿using System;

namespace CastEx
{
    public class A { }
    public class B : A { }

    public sealed class UsoInvalidCastException
    {
        public static void Main()
        {
            try
            {
                A a = new A();

                // La conversión de superclase a subclase 
                // en una jerarquía de herencia no está 
                // permitada.

                // El siguiente intento de conversión generará 
                // la excepción InvalidCastException:
                B b = (B)a;
            }
            catch (InvalidCastException ice)
            {
                Console.WriteLine("Mensaje de error: `{0}`", ice.Message);
            }
            //Los intentos fallidos de conversión implícita o explícita de tipos (e.g., interfaz, clase) generan la excepción InvalidCastException [8].
        }
    }
}
