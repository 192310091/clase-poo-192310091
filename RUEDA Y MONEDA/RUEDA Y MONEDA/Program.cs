﻿using System;

namespace RUEDA_Y_MONEDA
{
    class Program
    {
        static void Main(string[] args)
        {
            Operacion o = new Operacion();
            o.rueda();
            o.moneda();
            Console.ReadLine();
        }
    }
    class Operacion
    {
        public int radioR = 10, radioM = 1;
        public void rueda()
        {
            Console.WriteLine("RUEDA");
            Console.WriteLine("El area es:" + (3.1416 * Math.Pow(radioR, 2)));
            Console.WriteLine("El perimetro es:" + (3.1416 * (2 * radioR)));
        }
        public void moneda()
        {
            Console.WriteLine("MONEDA");
            Console.WriteLine("El area es:" + (3.1416 * Math.Pow(radioM, 2)));
            Console.WriteLine("El perimetro es:" + (3.1416 * (2 * radioM)));
        }
    }
}




