﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HerenciaSimple
{
    class Punto2D
    {
        public int X;
        public int Y;
    }
    class Punto3D : Punto2D
    {
        public int Z;
    }
}
