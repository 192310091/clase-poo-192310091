﻿using System;

namespace HerenciaSimple
{
    class Program
    {
        static void Main(string[] args)
        {
            Punto2D p2d = new Punto2D();
            Punto3D p3d = new Punto3D();
            Console.WriteLine(p2d.X = 100);
            Console.WriteLine(p2d.Y = 200);
            Console.WriteLine(p3d.X = 150);
            Console.WriteLine(p3d.Y = 250);
            Console.WriteLine(p3d.Z = 350);
            Console.ReadLine();
        }
    }
}
