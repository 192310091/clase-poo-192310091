﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRACTICA_2
{
    class TV
    {
        public int tamanio = 0;
        public int volumen = 0;
        public string color = "";
        public int brillo = 0;
        public int contraste = 0;
        public string marca = "";

        public void setTamanio(int tamanio)
        {
            this.tamanio = tamanio;
        }
        public int getTamanio()
        {
            return this.tamanio;
        }

        public void setVolumen(int volumen)
        {
            this.volumen = volumen;
        }
        public int getVolumen()
        {
            return this.volumen;
        }

        public void setColor(string color)
        {
            this.color = color;
        }
        public string getColor()
        {
            return this.color;
        }

        public void setBrillo(int brillo)
        {
            this.brillo = brillo;
        }
        public int getBrillo()
        {
            return this.brillo;
        }

        public void setContraste(int contraste)
        {
            this.contraste = contraste;
        }
        public int getContraste()
        {
            return this.contraste;
        }

        public void setMarca(string marca)
        {
            this.marca = marca;
        }
        public string getMarca()
        {
            return this.marca;
        }
    }
}
