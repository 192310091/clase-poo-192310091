﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PRACTICA_2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnClase_Click(object sender, EventArgs e)
        {
            TV TV1 = new TV();

            TV1.setTamanio(30);
            int tamanioTv1 = TV1.getTamanio();
            TV1.setVolumen(30);
            int volumenTv1 = TV1.getVolumen();
            TV1.setColor("Negro");
            string colorTv1 = TV1.getColor();
            TV1.setBrillo(100);
            int brilloTv1 = TV1.getBrillo();
            TV1.setContraste(50);
            int contrasteTv1 = TV1.getContraste();
            TV1.setMarca("LG");
            string marcaTv1 = TV1.getMarca();

            MessageBox.Show("El tamaño de la TV1 es de: " + tamanioTv1.ToString());
            MessageBox.Show("El volumen de la TV1 es de: " + volumenTv1.ToString());
            MessageBox.Show("El color de la TV1 es de: " + colorTv1.ToString());
            MessageBox.Show("El brillo de la TV1 es de: " + brilloTv1.ToString());
            MessageBox.Show("El contraste de la TV1 es de: " + contrasteTv1.ToString());
            MessageBox.Show("La marca de la TV1 es de: " + marcaTv1.ToString());

        }
    }
}
