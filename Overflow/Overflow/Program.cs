﻿using System;

namespace Overflow
{
    class Program
    {
        public sealed class UsoOverflowException
        {
            public static void Main()
            {
                checked
                {
                    int suma = Int32.MaxValue + Int32.Parse("1");
                }
            }
            //Para operaciones aritméticas o de conversiones que sobrepasan los límites de memoria de tipos de datos (e.g., enteros).
        }
    }
}
