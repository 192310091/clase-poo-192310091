﻿using System;

namespace NumeroAlCuadrado
{
    class Program
    {
        static void Main(string[] args)
        {
            int numero = 0;
            Console.WriteLine("INGRESE NUMERO A ELEVAR");
            numero = Convert.ToInt32(Console.ReadLine());
            double elevadoAlCuadrado = Math.Pow(numero, 2); // Elevarlo a la potencia 2
            Console.WriteLine(string.Format("{0} elevado al cuadrado es {1}", numero, elevadoAlCuadrado));
        }
    }
}
