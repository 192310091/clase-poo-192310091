﻿using System;

namespace PRACTICA_2
{
    class Program
    {
        static void Main(string[] args)
        {
            {
                Operacion o = new Operacion();
                o.Suma();
                Console.ReadLine();
            }
        }
        class Operacion
        {
            private int a, b;
            public void Suma()
            {
                Console.WriteLine("Valor de a:");
                a = int.Parse(Console.ReadLine());
                Console.WriteLine("Valor de b:");
                b = int.Parse(Console.ReadLine());
                Console.WriteLine("La suma es:" + (a + b));
            }
        }
    }
}


