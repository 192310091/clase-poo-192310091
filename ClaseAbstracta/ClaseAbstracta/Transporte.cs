﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClaseAbstracta
{
    abstract class Transporte
    {
        public abstract void Mantenimiento();
    }
    class Auto:Transporte
    {
        public override void Mantenimiento()
        {
            Console.WriteLine("*LISTADO DE MANTENIMIENTO DE AUTO*\n-Cambio de aceite\n-Chequeo de llantas\n-Bateria\n-Frenos\n-Amortiguadores");
        }
    }

    class Avion:Transporte
    {
        public override void Mantenimiento()
        {
            Console.WriteLine("*LISTADO DE MANTENIMIENTO DE AVION*\n-Llantas\n-Motor\n-Monitoreo de rendimiento");
        }
    }
}
