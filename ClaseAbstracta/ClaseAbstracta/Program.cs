﻿using System;

namespace ClaseAbstracta
{
    class Program
    {
        static void Main(string[] args)
        {
            Auto obj1 = new Auto();
            Avion obj2 = new Avion();

            obj1.Mantenimiento();
            obj2.Mantenimiento();
        }
    }
}
