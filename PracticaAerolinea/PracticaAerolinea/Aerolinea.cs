﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PracticaAerolinea
{
    class Aerolinea
    {
        public bool reservacionDisponible = true;
    }

    class Vuelo : Aerolinea
    {
        public char Destino;
        public char Salida;
        public char FechaVuelo;
        public char TiempoVuelo;
        public int PrecioVuelo;
        public int NumeroVuelo;
    }
}
