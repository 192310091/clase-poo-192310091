﻿using System;

namespace PracticaAerolinea
{
    class Program
    {
        static void Main(string[] args)
        {
            Aerolinea obj = new Aerolinea();
            Vuelo obj2 = new Vuelo();

            Console.WriteLine(obj.reservacionDisponible);

            Console.WriteLine("DESTINO: " + obj2.Destino);
            Console.ReadLine();
            Console.WriteLine("HORA DE SALIDA: " + obj2.Salida);
            Console.ReadLine();
            Console.WriteLine("FECHA DE VUELO: " + obj2.FechaVuelo);
            Console.ReadLine();
            Console.WriteLine("TIEMPO ESTIMADO DEL VIAJE: " + obj2.TiempoVuelo);
            Console.ReadLine();
            Console.WriteLine("COSTO DEL VUELO: " + obj2.PrecioVuelo);
            Console.ReadLine();
            Console.WriteLine("NUMERO DEL VUELO: " + obj2.NumeroVuelo);
            Console.ReadLine();
        }
    }
}
