﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PracticaPolimorfismo
{
    abstract class Empleado
    {
        public string primerNombre, apellidoPaterno, NSS;
        public double calcularSalario;
        public int sueldo, horas;
        public int ventasBrutas, tarifaComision;
        public int salarioBase;

        public abstract void salarioSemanal();

    }
    class Asalariado:Empleado
    {
        public override void salarioSemanal()
        {
            Console.WriteLine("Ingrese su primer nombre");
            primerNombre = Console.ReadLine();
            Console.WriteLine("Ingrese su apellido paterno");
            apellidoPaterno = Console.ReadLine();
            Console.WriteLine("Ingrese su No. de Seguro Social");
            NSS = Console.ReadLine();
            Console.WriteLine("-EMPLEADO ASALARIADO-\n" + primerNombre +" "+ apellidoPaterno + "\n" + NSS);
        }
    }
    class PorHoras:Empleado
    {
        public override void salarioSemanal()
        {
            Console.WriteLine("Ingrese sueldo");
            sueldo = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Ingrese horas trabajadas");
            horas = Convert.ToInt32(Console.ReadLine());

            if (horas <= 40)
            {
                calcularSalario = sueldo * horas;

                if (horas > 40)
                {
                    calcularSalario = 40 * sueldo + (horas - 40) * sueldo * 1.5;
                }
            }
        }
    }
    class PorComision:Empleado
    {
        public override void salarioSemanal()
        {
            Console.WriteLine("Ingrese las ventas brutas");
            ventasBrutas = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Ingrese tarifas de comision");
            tarifaComision = Convert.ToInt32(Console.ReadLine());
            calcularSalario = tarifaComision * ventasBrutas;
        }
    }
    class EmpleadoBaseMasComision:Empleado
    {
        public override void salarioSemanal()
        {
            Console.WriteLine("Ingrese sueldo base");
            salarioBase = Convert.ToInt32(Console.ReadLine());
            calcularSalario = (tarifaComision * ventasBrutas) + salarioBase;

            Console.WriteLine("SALARIO SEMANAL: $" + calcularSalario);
        }
    }
}
