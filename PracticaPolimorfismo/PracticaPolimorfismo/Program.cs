﻿using System;

namespace PracticaPolimorfismo
{
    class Program
    {
        static void Main(string[] args)
        {
            Asalariado obj = new Asalariado();
            obj.salarioSemanal();

            PorHoras obj2 = new PorHoras();
            obj2.salarioSemanal();

            PorComision obj3 = new PorComision();
            obj3.salarioSemanal();

            EmpleadoBaseMasComision obj4 = new EmpleadoBaseMasComision();
            obj4.salarioSemanal();
        }
    }
}
