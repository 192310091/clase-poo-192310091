﻿using System;

namespace IndexOut
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                int[] values1 = { 3, 6, 9, 12, 15, 18, 21 };
                int[] values2 = new int[6];

                // Assign last element of the array to the new array.
                values2[values1.Length - 1] = values1[values1.Length - 1];
            }
            catch(IndexOutOfRangeException e)
            {
                Console.WriteLine(e);
            }
        //Esta excepción significa que estás intentando (directa o indirectamente) acceder (para leer o escribir) a un Array usando un índice inválido. El índice es inválido cuando es menor que el límite inferior del Array o mayor o igual que el número de elementos que contiene.
        }
    }
}
