﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EXAMEN_PRACTICO_U1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CELULAR cel1 = new CELULAR();

            cel1.setColor("azul");
            string colorCel1 = cel1.getColor();
            cel1.setMarca("Motorola");
            string marcaCel1 = cel1.getMarca();
            cel1.setCapacidad(64);
            int capacidadCel1 = cel1.getCapacidad();
            cel1.setPrecio(4500);
            int precioCel1 = cel1.getPrecio();

            MessageBox.Show("El color del celular es: " + colorCel1.ToString());
            MessageBox.Show("La marca del celular es: " + marcaCel1.ToString());
            MessageBox.Show("La capacidad del celular es de: " + capacidadCel1.ToString()+("GB"));
            MessageBox.Show("El precio del celular es: $" + precioCel1.ToString()+(" pesos"));
        }
    }
}
