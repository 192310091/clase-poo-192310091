﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EXAMEN_PRACTICO_U1
{
    class CELULAR
    {
        public string marca = "";
        public string color = "";
        public int capacidad = 0;
        public int precio = 0;

        public void setMarca (string marca)
        {
            this.marca = marca;
        }
        public string getMarca()
        {
            return this.marca;
        }
        public void setColor(string color)
        {
            this.color = color;
        }
        public string getColor()
        {
            return this.color;
        }
        public void setCapacidad(int capacidad)
        {
            this.capacidad = capacidad;
        }
        public int getCapacidad()
        {
            return this.capacidad;
        }
        public void setPrecio(int precio)
        {
            this.precio = precio;
        }
        public int getPrecio()
        {
            return this.precio;
        }
    }
}
