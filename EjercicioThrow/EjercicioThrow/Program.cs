﻿using System;

namespace EjercicioThrow
{
    class Ejemplo
    {
        static void Main(string[] args)
        {
            int a, b, c = 0;
            Console.WriteLine("Leer a  y  b"); a = int.Parse(Console.ReadLine()); b = int.Parse(Console.ReadLine()); try
            {
                Ejemplo e = new Ejemplo(); c = e.CalculaDivision(a, b);
            }
            catch (Exception x)
            {
                Console.WriteLine(x.Message);
            }
            finally
            {
                Console.WriteLine(a + "/" + b + "=" + c);
            }
            Console.ReadKey();
        }
        public int CalculaDivision(int numerador, int denominador)
        {
            if (denominador == 0) throw new Exception("El denominador NO debe ser cero");
            else
                return (numerador / denominador);
        }
    }
}
