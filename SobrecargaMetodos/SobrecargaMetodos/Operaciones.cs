﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SobrecargaMetodos
{
    class Operaciones
    {
        public int Suma(int a, int b)
        {
            int resultado = a + b;
            return resultado;
        }

        public double Suma(double a, double b, double c)
        {
            double resultado = a + b + c;
            return resultado;
        }

        public string Suma(string a, string b)
        {
            string resultado = a + b;
            return resultado;
        }

        public float Suma(float a, float b, float c)
        {
            float resultado = a + b + c;
            return resultado;
        }
    }
}
