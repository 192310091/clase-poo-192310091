﻿using System;

namespace SobrecargaMetodos
{
    class Program
    {
        static void Main(string[] args)
        {
            Operaciones obj = new Operaciones();
            int resultado = obj.Suma(5, 9);
            Console.WriteLine("Resultado metodo 1: {0}", resultado);
            double resultado2 = obj.Suma(5.233, 9.567, 3.1416);
            Console.WriteLine("Resultado metodo 2: {0}", resultado2);
            string resultado3 = obj.Suma("1", "8");
            Console.WriteLine("Resultado metodo 3: {0}", resultado3);
            float resultado4 = obj.Suma(3.9f, 8.9f, 8.9f);
            Console.WriteLine("Resultado metodo 4: {0}", resultado4);
        }
    }
}
