﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Examen3
{
    class Clase2
    {
        public int Valor1 = 0;

        public Clase2()
        {
            this.Valor1 = 0;
            Console.WriteLine("CONSTRUCTOR");
        }
        public int metodo1(int a)
        {
            Console.WriteLine("Metodo 1");
            return this.Valor1 = a;
        }

        private void metodo2()
        {
            Console.WriteLine("Metodo 2, inaccesible");
        }

        protected void metodo3()
        {
            Console.WriteLine("Metodo 3");
        }
        public void AccesoMetodo2()
        {
            metodo2();
        }
        ~Clase2()
        {
            Console.WriteLine("DESTRUCTOR");
        }
    }
    class Hijo : Clase2
    {
        public Hijo()
        {
            this.Valor1 = 0;
            Console.WriteLine("CONSTRUCTOR");
        }
        public void AccesoMetodo3()
        {
            metodo3();
        }
        public int metodo5(int b)
        {
            Console.WriteLine("Metodo 5");
            return this.Valor1 = b;
        }
        ~Hijo()
        {
            Console.WriteLine("DESTRUCTOR");
        }
    }
}
