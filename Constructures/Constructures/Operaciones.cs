﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Constructures
{
    class Operaciones
    {
        private int Multa;

        public Operaciones(int multa)
        {
            this.Multa = multa;
        }

        public int CalcularMulta()
        {
            int Resultado = 0;
            switch (this.Multa)
            {
                case 0:
                    Resultado = 180;
                    break;
                case 1:
                    Resultado = 150;
                    break;
                case 2:
                    Resultado = 200;
                    break;
                case 3:
                    Resultado = 300;
                    break;
                default:
                    Resultado = 0;
                    break;
            }
            return Resultado;
        }
    }
}
