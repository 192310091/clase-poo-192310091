﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Constructures
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btnCalcularMulta_Click(object sender, EventArgs e)
        {
            int indice = cbxMultas.SelectedIndex;
            Operaciones obj = new Operaciones(indice);
            lblResultado.Text = Convert.ToString(obj.CalcularMulta());
        }
    }
}
