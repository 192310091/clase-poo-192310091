﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PracticaHerencia
{
    class Herencia
    {
        //se declaran los atributos que podran ser accedidos por la clase que hereda
        //el modificador de acceso PRIVATE es la excepcion
        public int Atributo1;
        private int Atributo2;
        protected int Atributo3;

        //se declaran los metodos que de igual manera heredara otra clase
        public void metodo1()
        {
            Console.WriteLine("Este es el metodo 1 de herencia");
        }

        private void metodo2()
        {
            Console.WriteLine("Este metodo no se puede acceder");
        }

        protected void metodo3()
        {
            Console.WriteLine("Este es el metodo 3 de herencia");
        }

        public void AccesoMetodo2()
        {
            metodo2();
        }
    }

    //la clase hijo hereda las propiedades de la clase madre
    class Hijo:Herencia
    {
        public void AccesoMetodo3()
        {
            metodo3();
        }
    }
}
