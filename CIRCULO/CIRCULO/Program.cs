﻿using System;

namespace CIRCULO
{
    class Program
    {
        static void Main(string[] args)
        {
            Operacion o = new Operacion();
            o.calcular();
            Console.ReadLine();
        }
    }
    class Operacion
    {
        private double radio;
        public void calcular()
        {
            Console.WriteLine("Valor de radio:");
            radio = double.Parse(Console.ReadLine());
            Console.WriteLine("El area es:" + (3.1416*Math.Pow(radio,2)));
            Console.WriteLine("El perimetro es:" + (3.1416 * (2*radio)));
        }
    }
}