﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PracticaVuelo
{
    class MonitoreoViaje
    {
        public char nro_monitoreo;
        public char diagnostico_pac;
        public char hora;
        public char pulso;
        public char presion;
        public char lat_fetales;
        public char nro_contracciones;
        public char hem_cantidad;
        public char convulciones_hora;
        public char paro_cardio;
        public char otros;
    }

    class AccionRealizada:MonitoreoViaje
    {
        public char cod_accion;
        public char descripcion;
        public char tipo;

        public void Listar()
        {
           
        }

        public void Actualizar()
        {

        }

        public void Guardar()
        {

        }
    }
}
