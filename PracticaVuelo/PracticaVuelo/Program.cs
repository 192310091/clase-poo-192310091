﻿using System;

namespace PracticaVuelo
{
    class Program
    {
        static void Main(string[] args)
        {
            MonitoreoViaje obj1 = new MonitoreoViaje();

            Console.WriteLine("NUMERO DE MONITOREO: " + obj1.nro_monitoreo);
            Console.ReadLine();
            Console.WriteLine("DIAGNOSTICO: " + obj1.diagnostico_pac);
            Console.ReadLine();
            Console.WriteLine("HORA: " + obj1.hora);
            Console.ReadLine();
            Console.WriteLine("PULSO: " + obj1.pulso);
            Console.ReadLine();
            Console.WriteLine("PRESION: " + obj1.presion);
            Console.ReadLine();
            Console.WriteLine("LAT. FETALES: " + obj1.lat_fetales);
            Console.ReadLine();
            Console.WriteLine("NUMERO DE CONTRACCIONES: " + obj1.nro_contracciones);
            Console.ReadLine();
            Console.WriteLine("HEM. CANTIDAD: " + obj1.hem_cantidad);
            Console.ReadLine();
            Console.WriteLine("CONVULCIONES POR HORA: " + obj1.convulciones_hora);
            Console.ReadLine();
            Console.WriteLine("PARO CARDIO: " + obj1.paro_cardio);
            Console.ReadLine();
            Console.WriteLine("OTROS: " + obj1.otros);
            Console.ReadLine();

            AccionRealizada obj2 = new AccionRealizada();

            Console.WriteLine("NUMERO DE MONITOREO: " + obj1.nro_monitoreo);
            Console.WriteLine("COD. ACCION: " + obj2.cod_accion);
            Console.ReadLine();
            Console.WriteLine("DESCRIPCION: " + obj2.descripcion);
            Console.ReadLine();
            Console.WriteLine("TIPO: " + obj2.tipo);
            Console.ReadLine();
        }
    }
}
