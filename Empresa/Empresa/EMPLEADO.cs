﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Empresa
{
    class EMPLEADO
    {
        public int Horas, HorasEx;
    }

    class INTENDENTES:EMPLEADO
    {
        public void salarioIntendente()
        {
            Console.WriteLine("-INTENDENTE-\nIngrese las horas trabajadas");
            Horas = int.Parse(Console.ReadLine());
            Console.WriteLine("Ingrese las horas extras trabajadas");
            HorasEx = int.Parse(Console.ReadLine());
            Console.WriteLine("El salario del intendente es de: $" + ((Horas * 20) + (HorasEx * 20)));
        }
    }

    class PRODUCCION:EMPLEADO
    {
        public void salarioProduccion()
        {
            Console.WriteLine("-PRODUCCION-\nIngrese las horas trabajadas");
            Horas = int.Parse(Console.ReadLine());
            Console.WriteLine("Ingrese las horas extras trabajadas");
            HorasEx = int.Parse(Console.ReadLine());
            Console.WriteLine("El salario de la produccion es de: $" + ((Horas * 35) + (HorasEx * 35)));
        }
    }

    class ADMINISTRATIVOS : EMPLEADO
    {
        public void salarioAdministrativos()
        {
            Console.WriteLine("-ADMINISTRATIVOS-\nIngrese las horas trabajadas");
            Horas = int.Parse(Console.ReadLine());
            Console.WriteLine("Ingrese las horas extras trabajadas");
            HorasEx = int.Parse(Console.ReadLine());
            Console.WriteLine("El salario de los administrativos es de: $" + ((Horas * 50) + (HorasEx * 50)));
        }
    }

    class GERENCIAS : EMPLEADO
    {
        public void salarioGerencia()
        {
            Console.WriteLine("-GERENCIA-\nIngrese las horas trabajadas");
            Horas = int.Parse(Console.ReadLine());
            Console.WriteLine("Ingrese las horas extras trabajadas");
            HorasEx = int.Parse(Console.ReadLine());
            Console.WriteLine("El salario de la gerencia es de: $" + ((Horas * 120) + (HorasEx * 120)));
        }
    }

    class DUEÑOS : EMPLEADO
    {
        public void salarioDueños()
        {
            Console.WriteLine("-DUEÑOS-\nIngrese las horas trabajadas");
            Horas = int.Parse(Console.ReadLine());
            Console.WriteLine("Ingrese las horas extras trabajadas");
            HorasEx = int.Parse(Console.ReadLine());
            Console.WriteLine("El salario de los dueños es de: $" + ((Horas * 200) + (HorasEx * 200)));
        }
    }
}
