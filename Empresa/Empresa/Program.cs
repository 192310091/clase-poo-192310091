﻿using System;

namespace Empresa
{
    class Program
    {
        static void Main(string[] args)
        {
            EMPLEADO salarios = new EMPLEADO();

            INTENDENTES sal1 = new INTENDENTES();
            sal1.salarioIntendente();

            PRODUCCION sal2 = new PRODUCCION();
            sal2.salarioProduccion();

            ADMINISTRATIVOS sal3 = new ADMINISTRATIVOS();
            sal3.salarioAdministrativos();

            GERENCIAS sal4 = new GERENCIAS();
            sal4.salarioGerencia();

            DUEÑOS sal5 = new DUEÑOS();
            sal5.salarioDueños();
        }
    }
}
