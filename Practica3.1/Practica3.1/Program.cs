﻿using System;

namespace Practica3._1
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] a = new int[4]; 
            float[] b = new float[4];
            OrdVar p = new OrdVar();
            Console.WriteLine("Elementos del arreglo:"); 
            for (int i = 0; i < a.Length; i++)
                a[i] = Convert.ToInt32(Console.ReadLine()); 
            Console.WriteLine("Elementos del arreglo:"); 
            for (int j = 0; j < b.Length; j++) 
                b[j] = Convert.ToInt32(Console.ReadLine());
            p.Ordenar(a);
            p.Ordenar(b); 
            Console.ReadLine();
        }
       

    }
}

