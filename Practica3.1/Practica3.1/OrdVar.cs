﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Practica3._1
{
    class OrdVar
    {
        public void Ordenar(params int[] a)
        {
            Console.WriteLine("-------------ORDENA-----------");
            Array.Sort(a); foreach (int i in a) Console.WriteLine(+i);
        }
        public void Ordenar(params float[] b)
        {
            //reordena 
            Console.WriteLine("-------------REORDENA-----------");
            Array.Reverse(b);
            foreach (int i in b) Console.WriteLine(+i);
        }
    }
}
