﻿using System;

namespace FormatEx
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Ingrese un valor:");
            string linea = Console.ReadLine();
            var num = int.Parse(linea);
            var cuadrado = num * num;
            Console.WriteLine($"El cuadrado de {num} es {cuadrado}");
            Console.ReadKey();
            //Si ejecutamos el programa y el operador ingresa caracteres no numéricos en lugar de un entero se genera una excepción y el programa se detiene en forma inmediata
        }
    }
}
