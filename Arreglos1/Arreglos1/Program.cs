﻿using System;

namespace Arreglos1
{
    class Program
    {
        static void Main(string[] args)
        {
            int suma = 0;
            Console.WriteLine("INGRESE EL TAMAÑO DEL ARREGLO");
            int[] CatalogoNumeros = new int[Convert.ToInt32(Console.ReadLine())];

            for (int i = 0; i < CatalogoNumeros.Length; i++)
            {
                Console.WriteLine("Ingresa un numero");
                int num = Convert.ToInt32(Console.ReadLine());
                CatalogoNumeros[i] = num;
            }
            foreach (int val in CatalogoNumeros)
            {
                Console.WriteLine(val);
                suma = suma + val;
            }
            Console.WriteLine("\n" + "LA SUMA DEL ARREGLO ES: " + suma);
            Console.ReadKey();
        }
    }
}
